class ElasticBaseQuery {
    get query_body() {
        let query = {
            'query': {
                'bool': {
                    'must': this._filter_must,
                    'should': this._filter_should,
                    'must_not': this._filter_must_not,
                }
            }
        };

        if (this._start !== null)
            query.from = this._start;

        if (this._size !== null)
            query.size = this._size;

        if (this._sort !== null)
            query.sort = this._sort;

        if (this._filter_must.length > 0 && this._filter_should.length > 0)
            query.query.bool.minimum_should_match = 1;

        if (this._minimum_should_match !== null)
            query.query.bool.minimum_should_match = this._minimum_should_match;

        if (this._aggs !== null)
            query.aggs = this._aggs;

        if (this._search_after !== null)
            query.search_after = this._search_after;

        return query;
    }
    get index() {
        return this._index;
    }
    get filter_must() {
        return this._filter_must;
    }
    get filter_should() {
        return this._filter_should;
    }
    get filter_must_not() {
        return this._filter_must_not;
    }
    get sort() {
        return this._sort;
    }
    get aggs() {
        return this._aggs;
    }
    get start() {
        return this._start;
    }
    get search_after() {
        return this._search_after;
    }
    set start(value) {
        if (typeof value !== 'number')
            throw '\'start\' parameter must be number';

        this._start = value;
    }
    get size(){
        return this._size;
    }
    set size(value) {
        if (typeof value !== 'number')
            throw '\'size\' parameter must be number';

        this._size = value;
    }
    set search_after(value) {
        this._search_after = value;
    }
    get minimum_should_match (){
        return this._minimum_should_match;
    }
    set minimum_should_match(value) {
        if (value !== null && typeof value !== 'number')
            throw '\'minimum should match\' parameter must be number';

        this._minimum_should_match = value;
    }
    get server() {
        return this._server;
    }
    set server(value) {
        this._server = value;
    }
}
module.exports = ElasticBaseQuery;
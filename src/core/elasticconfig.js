const { Client } = require('@elastic/elasticsearch');

class ElasticConfig {
    constructor(options) {
        this.connection_config = options;
        this.elasticsearch = new Client(options);
        this.elasticsearch.globalError = options.error || false;
        this.elasticsearch.globalPrefix = options.prefix || "dev_";
        return this.elasticsearch;
    }
    get config (){
        return this.connection_config;
    }
    set config(options){
        this.connection_config = options;
    }
    set globalError(value){
        this.elasticsearch.globalError = value;
    }
    get globalError(){
        return this.elasticsearch.globalError;
    }
    set globalPrefix(value){
        this.elasticsearch.globalPrefix = value;
    }
    get globalPrefix(){
        return this.elasticsearch.globalPrefix;
    }
}

module.exports = ElasticConfig;
